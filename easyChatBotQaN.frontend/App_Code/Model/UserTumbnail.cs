﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de UserTumbnail
/// </summary>
public class UserTumbnail
{
	public int Id { get; set; }
	public string Name { get; set; }
	public string Username { get; set; }
	public string Tumbnail { get; set; }
	public string Email { get; set; }
}