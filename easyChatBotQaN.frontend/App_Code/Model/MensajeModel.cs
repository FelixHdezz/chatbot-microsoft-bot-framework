﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de MensajeModel
/// </summary>
public class MensajeModel
{
	public string Name { get; set; }
	public string UserName { get; set; }
	public string Question { get; set; }
	public string Answer { get; set; }
	public string Email { get; set; }
	public int IdArea { get; set; }
	public string Url { get; set; }

	//
	public string ContactName { get; set; }
	public string ContactEmail { get; set; }
}