﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Descripción breve de EmailServices
/// </summary>
public class EmailServices 
{
	#region Private Properties

	private string MailAddress => ConfigurationManager.AppSettings["mailNotifier"];

	#endregion Private Properties

	#region Public Methods

	public void SendAsync(IdentityMessage message)
	{
		SendAsync(message, new List<LinkedResource>(), new List<Attachment>());
	}

	public void SendAsync(IdentityMessage identityMessage, List<LinkedResource> linkedResources, List<Attachment> attachments)
	{
		var client = new SmtpClient();

		var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
		string strHost = smtpSection.Network.Host;
		int port = smtpSection.Network.Port;
		string strUserName = smtpSection.Network.UserName;
		string strFromPass = smtpSection.Network.Password;

		SmtpClient smtp = new SmtpClient(strHost, port);
		NetworkCredential cert = new NetworkCredential(strUserName, strFromPass);
		smtp.Credentials = cert;
		//smtp.EnableSsl = true;

		MailMessage msg = new MailMessage(smtpSection.From, identityMessage.Destination);
		msg.Subject = identityMessage.Subject;
		msg.IsBodyHtml = true;
		msg.Body = identityMessage.Body;

		smtp.Send(msg);
	}

	#endregion Public Methods
}