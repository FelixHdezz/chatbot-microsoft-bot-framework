﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de ActiveDirectory
/// </summary>
public class ActiveDirectory
{
	public string getUserName()
	{
		return HttpContext.Current.User.Identity.Name.ToString();
	}

	public UserTumbnail getUserTumnail(string userName)
	{
		SearchActiveDirectory _searchAD = new SearchActiveDirectory();

		return _searchAD.AuthenticateUser(userName);
	}
}