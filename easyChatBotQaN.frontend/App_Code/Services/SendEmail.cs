﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Descripción breve de SendEmail
/// </summary>
public class SendEmail
{
	private string MailAddress => ConfigurationManager.AppSettings["mailNotifier"];
	private string Destination => ConfigurationManager.AppSettings["mailContact"];
	public string SendEmailToQuestion(MensajeModel _mensajeModel)
	{
		var variables = new Dictionary<string, string>();
		variables.Add("{{ContactName}}", _mensajeModel.ContactName);
		variables.Add("{{Name}}", _mensajeModel.Name);
		variables.Add("{{Email}}", _mensajeModel.Email);
		variables.Add("{{Message}}", _mensajeModel.Question);
		variables.Add("{{ConfirmationHref}}", _mensajeModel.Url);
		string _msg = string.Empty;
		try
		{
			var identityMessage = GetIdentityMessage(_mensajeModel.ContactEmail, "Contacto", ResourcePaths.SEND_MESSAGE_TEMPLATE, variables);

			EmailServices emailServices = new EmailServices();
			emailServices.SendAsync(identityMessage);

			_msg = "El mensaje se envio existosamente.";
		}
		catch (Exception ex) {
			_msg = ex.Message.ToString();
		}
		return _msg;
	}

	public string SendEmailToUser(MensajeModel _mensajeModel) {
		var variables = new Dictionary<string, string>();

		variables.Add("{{ContactName}}", _mensajeModel.ContactName);
		variables.Add("{{Question}}", _mensajeModel.Question);
		variables.Add("{{Answer}}", _mensajeModel.Answer);
		string _msg = string.Empty;
		try
		{
			var identityMessage = GetIdentityMessage(_mensajeModel.ContactEmail, "Contacto", ResourcePaths.SEND_MESSAGE_USER_TEMPLATE, variables);
			EmailServices emailServices = new EmailServices();
			emailServices.SendAsync(identityMessage);
			_msg = "El mensaje se envio existosamente.";
		}
		catch (Exception ex)
		{
			_msg = ex.Message.ToString();
		}
		return _msg;
	}

	public void sendMessage() {

	}

	protected IdentityMessage GetIdentityMessage(string destination, string subject, string templatePath, Dictionary<string, string> variables)
	{
		var body = GetBodyFromTemplate(templatePath, variables);
		return new IdentityMessage
		{
			Destination = destination,
			Subject = subject,
			Body = body
		};
	}

	private string GetBodyFromTemplate(string templatePath, Dictionary<string, string> variables)
	{
		var body = GetBody(templatePath);
		foreach (var pair in variables)
		{
			body = body.Replace(pair.Key, pair.Value);
		}
		return body;
	}

	protected string GetBody(string templatePath)
	{
		using (StreamReader reader = new StreamReader(Map(templatePath)))
		{
			return reader.ReadToEnd();
		}
	}

	protected string Map(string relativePath)
	{
		return HttpContext.Current.Request.MapPath(relativePath);
	}
}