﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de SearchActiveDirectory
/// </summary>
public class SearchActiveDirectory
{
	public DirectoryEntry pvc;

	public ArrayList getDataFromAD(string userName)
	{
		ArrayList result = new ArrayList();

		DirectoryEntry myLdapConnection = createDirectoryEntry();

		DirectorySearcher search = new DirectorySearcher(myLdapConnection);
		//search.Filter = "(SAMAccountName=" + userName + "*)";

		search.Filter = "(SAMAccountName=" + userName + ")";
		DirectoryEntry UserEntry = null;
		foreach (SearchResult resEntUser in search.FindAll())
		{
			UserEntry = resEntUser.GetDirectoryEntry();
			if (UserEntry.Properties["mail"].Value != null)
			{
				result.Add(UserEntry.Properties["mail"].Value.ToString() + "||" + UserEntry.Properties["SN"].Value.ToString() + " " + UserEntry.Properties["givenName"].Value.ToString() + "(" + UserEntry.Properties["SAMAccountName"].Value.ToString() + ")");
			}
		}
		return result;
	}

	public List<string> getUserAdGroups(string userName)
	{
		List<string> result = new List<string>();

		DirectoryEntry myLdapConnection = createDirectoryEntry();

		DirectorySearcher search = new DirectorySearcher(myLdapConnection);
		search.Filter = "(&(samaccountname=" + userName + ")(objectClass=person))";
		search.PropertiesToLoad.Add("memberof");

		SearchResult resEntUser = search.FindOne();

		if (resEntUser != null)
		{
			foreach (string str in resEntUser.Properties["memberof"])
			{
				string str2 = str.Substring(str.IndexOf("=") + 1, str.IndexOf(",") - str.IndexOf("=") - 1);
				result.Add(str2);
			}
		}

		return result;
	}

	private static DirectoryEntry createDirectoryEntry()
	{
		// create and return new LDAP connection with desired settings

		//Domain aun no esta declarada
		DirectoryEntry ldapConnection = new DirectoryEntry("LDAP://Net1.local");
		//ldapConnection.AuthenticationType = AuthenticationTypes.Secure;
		return ldapConnection;
	}

	public void getAD()
	{
		string username = HttpContext.Current.User.Identity.Name.Split(new char[] { '\\' })[1];

		DirectoryEntry myLdapConnection = createDirectoryEntry();

		DirectorySearcher search = new DirectorySearcher(myLdapConnection);
		search.Filter = "(SAMAccountName=" + username + ")";
		var dsResults = search.FindOne();
		var myEntry = dsResults.GetDirectoryEntry();

		pvc = myEntry;
	}

	public void getActiveD(string user)
	{
		string username = user;

		DirectoryEntry myLdapConnection = createDirectoryEntry();

		DirectorySearcher search = new DirectorySearcher(myLdapConnection);
		search.Filter = "(SAMAccountName=" + username + ")";
		var dsResults = search.FindOne();
		var myEntry = dsResults.GetDirectoryEntry();
		pvc = myEntry;
	}

	public string getDomianFull(string searchName)
	{
		string _domain = string.Empty;
		DirectoryContext context = new DirectoryContext(DirectoryContextType.Domain, searchName);
		Domain domain = Domain.GetDomain(context);
		return _domain;
	}

	public string getDomainList()
	{
		DirectoryEntry en = new DirectoryEntry("LDAP://");
		// Search for objectCategory type "Domain"
		DirectorySearcher srch = new DirectorySearcher("objectCategory=Domain");
		SearchResultCollection coll = srch.FindAll();
		// Enumerate over each returned domain.
		foreach (SearchResult rs in coll)
		{
			ResultPropertyCollection resultPropColl = rs.Properties;
			foreach (object domainName in resultPropColl["name"])
			{
				var _domain = domainName.ToString();
			}
		}
		return "test";
	}

	public UserTumbnail AuthenticateUser(string username)
	{
		UserTumbnail _userData = new UserTumbnail();
		var LdapPath = "LDAP://net1.local";
		DirectoryEntry entry = new DirectoryEntry(LdapPath);
		try
		{
			// Bind to the native AdsObject to force authentication.
			//Object obj = entry.NativeObject;
			DirectorySearcher search = new DirectorySearcher(entry);
			search.Filter = "(SAMAccountName=" + username.Split('\\').Last() + ")";
			search.PropertiesToLoad.Add("cn");
			search.PropertiesToLoad.Add("mail");
			SearchResult result = search.FindOne();
			if (null == result)
			{
				return new UserTumbnail();
			}
			// Update the new path to the user in the directory
			LdapPath = result.Path;
			_userData.Name = (String)result.Properties["cn"][0];
			_userData.Email = (String)result.Properties["mail"][0];
		}
		catch (Exception ex)
		{
			return new UserTumbnail();
		}
		return _userData;
	}
}