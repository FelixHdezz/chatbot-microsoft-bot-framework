﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de ResourcePaths
/// </summary>
public static class ResourcePaths
{
	public const string CONFIRM_ACCOUNT_TEMPLATE = "~/Content/EmailTemplate/confirm_account_template.html";
	public const string LOGO = "~/Content/Images/logo.png";
	public const string SEND_MESSAGE_TEMPLATE = "~/Content/EmailTemplate/send_message_template.html";
	public const string SEND_MESSAGE_USER_TEMPLATE = "~/Content/EmailTemplate/send_message_user_template.html";
}