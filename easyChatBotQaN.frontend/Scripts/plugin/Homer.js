﻿//Homer script global

//Publica una base de conocimiento mediante el keyId, carga de nuevas preguntas-respuestas o crear
//una base de conocimiento
var publish = function (_keyId, callback) {
    var _url = API_QANMAKER + '/' + _keyId;
    setTimeout(function () {
        _callAjax(_url, "POST", header()).done(function (response, statusText, xhr) {
            console.log("Response status publish ", xhr.status);
            if (xhr.status === 204) {
                //is status ok, return response
                return callback(response);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            //console return error server

            //Si el error es del servidor, enviar nuevamente los datos
            publish(_keyId, callback);
        });
    }, 2500);
};

//Obtiene el keiId de una base de conocimiento al api
var getKnowledgeBaseByName = function (_name, callback) {
    var url = API_QANMAKER;
    callAjax(url, "GET", null, header()).done(function (response, statusText, xhr) {
        if (response && xhr.status === 200) {
            var _KnowledgeBases = response.knowledgebases;
            for (var _index = 0; _index < _KnowledgeBases.length; _index++) {
                if (_KnowledgeBases[_index].name === _name) {
                    console.log(_KnowledgeBases[_index].name);
                    var _data = {
                        hostName: _KnowledgeBases[_index].hostName,
                        id: _KnowledgeBases[_index].id
                    };
                    return callback(_data);
                }
            }
            //Si aun no devuelve los datos el servidor, enviamos nuevamente
            getKnowledgeBaseByName(_name, callback);
        }
    });
};

//Header, credenciales para la conexion con qnamaker y azure
var header = function () {
    return _header = {
        'Ocp-Apim-Subscription-Key': SUBSCCRIPTION_KEY,
        'Content-Type': 'application/json'
    };
};

//funcion ajax global
var callAjax = function (_url, _type, _data, _headers) {
    return $.ajax({
        method: _type,
        contentType: "application/json",
        url: _url,
        data: _data,
        dataType: "json",
        async: true,
        crossDomain: true,
        headers: _headers
    });
};

var _callAjax = function (_url, _type, _headers) {
    return $.ajax({
        async: true,
        crossDomain: true,
        url: _url,
        method: _type,
        url: _url,
        headers: _headers,
        processData: false,
        data:""
    });
};

//funcion ajax global para envio de parametros por medio de fromdata
var callAjaxFormData = function (_url, _formData) {
    return $.ajax({
        url: _url,
        type: 'POST',
        contentType: false,
        processData: false,
        data: _formData
    });
};