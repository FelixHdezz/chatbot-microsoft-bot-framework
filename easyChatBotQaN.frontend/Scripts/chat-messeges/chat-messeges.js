﻿var chatMessenges = {
    init: function () {
        //if secction chat-messeges is hidden so to show
        $('section-chat-messege-welcome').removeClass('hidden');

        //Init load combobox areas
        chatMessenges.renderComboBox();

        //Show modal chat
        $("#addClass").click(chatMessenges.showModalChat);

        //Close modal chat
        $("#removeClass").click(chatMessenges.closeModalChat);

        $(document).on('click', '.render-chat-messeges', chatMessenges.renderChatMesseges);

        //$(document).on('keyup', '.css-1yaojre input', function (event) {
        //    if (event.keyCode === 13) {
        //        var _msgToSend = $(this).val();

        //        //cacha el mesansaje que devolvio el fot framework
        //        setTimeout(function () {
        //            chatMessenges.catchMesseges(_msgToSend);
        //        }, 1500);
        //    }
        //});

        $(document).on('click', '.wc-send', function (event) {
            //Otiene la pregunta que el usuario esta realizando antes de enviar al QaNMaker
            var _msgToSend = $('.wc-shellinput').val();

            //cacha el mesansaje que devolvio el fot framework
            setTimeout(function () {
                chatMessenges.catchMesseges(_msgToSend);
            }, 1900);
        });
    },

    renderComboBox: function (e) {
        var _URL = API_URL + "/api/areas";
        chatMessenges.callAjax(_URL, "GET").done(function (data) {
            if (data) {
                for (var _i = 0; _i < data.length; _i++) {
                    $('.select-area').append($('<option>', {
                        value: data[_i].id,
                        text: data[_i].area
                    }));
                }
            }
        }).fail(function (jqXHR, txtStatus, error) {
            //return: server error internal
            console.log('Server error internal: ', jqXHR.statusText);
        });
    },

    showModalChat: function (e) {
        var _url = API_URL + '/api/areas/getUserLogon';
        chatMessenges.callAjax(_url, "POST").done(function (resp) {
            if (resp) {
                //Init chat messeges
                console.log("user logged in: " + resp);
            }
        });

        e.preventDefault();
        $('#qnimate').addClass('popup-box-on');
    },

    closeModalChat: function (e) {
        e.preventDefault();
        $('#qnimate').removeClass('popup-box-on');
        $('.section-chat-messege-welcome').removeClass('hidden');
    },

    renderChatMesseges: function (e) {
        e.preventDefault();

        //Verifica que haya seleccionado alguna area
        if ($('.select-area').val() !== '0') {
            var _select = $('.select-area');
            $('.head-title').text('Bot ' + $('.select-area option[value="' + $(_select).val() + '"]').text());

            //Ocultar section de chat bienvenida
            $('.section-chat-messege-welcome').addClass('hidden');

            //render web chat - plugin bot framework
            chatMessenges.registerUserArea(function (response) {
                chatMessenges.renderWebChat();
            });
        }
    },

    registerUserArea: function (callback) {
        //Object data to send
        var data = chatMessenges.createObj();
        //API_URL
        var _url = API_URL + '/api/areas/registerUserArea';

        //call function ajax
        chatMessenges.callAjax(_url, "POST", JSON.stringify(data)).done(function (response) {
            if (response) {
                callback(response);
            }
        }).fail(function (jqXHR, txtStatus, error) {
            //return: server error internal
            console.log('Server error internal: ', jqXHR.statusText);
        });
    },

    renderWebChat: function () {
        //define user name
        var _area = $('.select-area').val();
        var _username = Name.replace(' (AMXEXT)', '').split(' ').pop(-1);

        //define bot name
        var _select = $('.select-area');
        var _botname = 'Bot ' + $('.select-area option[value="' + $(_select).val() + '"]').text();

        BotChat.App({
            directLine: { secret: "lLvp4wLTRxA.cwA.x3g.qgY95ZsLUfyykiunJdnu53oU9Z6E_KdJFKdVFCJqZmU" },
            user: { id: _username, userName: UserName, area: _area },
            bot: { id: _botname },
            resize: 'detect'
        }, document.getElementById("webchat"));
    },

    catchMesseges: function (_msgToSend) {
        //Obtiene el ultimo div
        var _lastChild = $('.wc-message-wrapper').last();

        //verifica si el ultimo row que se agrego no es del usuario
        if ($(_lastChild).find('.wc-message').not('.wc-message-from-me').length > 0) {
            var _findMesseges = $(_lastChild).find('.wc-message-content');
            var _returnMsg = $(_findMesseges).find('.format-markdown p').text();
            //Elimina acento al mensaje
            var _msg = chatMessenges.normalize(_returnMsg);

            //Verifica si el mensaje se retorno, el 
            if (_msg.toLowerCase() === 'lo siento, no entendi tu mensaje') {

                //Send question to api
                var _IdArea = $('.select-area').val();
                var _url = API_URL + '/api/contact/sendMessage/' + _IdArea;
                chatMessenges.callAjax(_url, "GET").done(function (response) {
                    if (response) {
                        $('#txtContactName').val(response.contactName);
                        $('#txtContactEmail').val(response.email);

                        //
                        chatMessenges.createObjToMessage(_msgToSend);
                        //Evento para enviar Email
                        $('.sendEmail').click();
                    }
                }).fail(function (jqXHR, txtStatus, error) {
                    //return: server error internal
                    console.log('Server error internal - Send message: ', jqXHR.statusText);
                });
            }
        }
    },

    createObjToMessage: function (_question) {
        var _IdArea = $('.select-area').val();
        var Url = window.location.href + "KnowledgeBase/LearningKnowledgeBase.aspx?IdArea=" + _IdArea + "&Question=" + _question + "&UserName=" + UserName;
        $('#txtUserName').val(Name.replace(' (AMXEXT)', ''));
        $('#txtUser').val(UserName);
        $('#txtEmailUser').val(UserEmail);
        $('#txtQuestion').val(_question);
        $('#txtUrl').val(Url);
        $('#txtIdArea').val(_IdArea);
    },

    createObj: function () {
        var obj = {
            idArea: $('.select-area').val(),
            name: Name.replace(' (AMXEXT)', '').split(' ').pop(-1),
            userName: UserName,
            email: UserEmail
        };

        //return object to send
        return obj;
    },

    //call api function ajax
    callAjax: function (_url, _type, _data) {
        return $.ajax({
            method: _type,
            contentType: "application/json",
            url: _url,
            data: _data,
            dataType: "json",
            async: true,
            crossDomain: true
        });
    },

    normalize: function (msg) {
        var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç",
            to = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
            mapping = {};

        for (var _i = 0, _j = from.length; _i < _j; _i++) {
            mapping[from.charAt(_i)] = to.charAt(_i);
        }

        if (mapping) {
            var ret = [];
            for (var i = 0, j = msg.length; i < j; i++) {
                var c = msg.charAt(i);
                if (mapping.hasOwnProperty(msg.charAt(i))) {
                    ret.push(mapping[c]);
                } else {
                    ret.push(c);
                }
            }
            return ret.join('');
        }
    },
};

//initilizate function javascript
$(chatMessenges.init);