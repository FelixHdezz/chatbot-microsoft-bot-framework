﻿var createKnowLedgeBase = {
    //function init
    init: function () {
        //
        createKnowLedgeBase.loaduUploader();

        $('#btnUploadSubmit').on('click', createKnowLedgeBase.upLoadFile);
    },

    loaduUploader: function (extension) {
        $("#uploadFiles form").attr("action", API_URL + "Photos//Upload");

        fileUploadApi = $('#uploadFiles input[name="files"]').fileuploader({
            enableApi: true,
            //extensions: extension,
            addMore: true
        });

        window.api = $.fileuploader.getInstance(fileUploadApi);
    },

    upLoadFile: function (event) {
        event.preventDefault();
        var $uploadForm = $("#uploadForm");
        
        var formData = new FormData();

        formData.append("NombreResponsable", $('#txt_nameResponsable').val());
        formData.append("Apellidos", $('#txt_lastName').val());
        formData.append("Telefono", $('#txt_phone').val());
        formData.append("Email", $('#txt_email').val());
        formData.append("NombreConocimiento", $('#txt_NameKnowledge').val());

        var $files = $("input[type=file]")[0].files;

        for (var i = 0; i < $files.length; i++) {
            if (createKnowLedgeBase.validateFilesToSend($files[i].name)) {
                formData.append("Archivo" + i, $files[i]);
            }
        }

        //call function ajax
        var _URL = API_URL + '/api/knowledgebase/createKnowledge';
        callAjaxFormData(_URL, formData).done(function (response, statusText, xhr) {
            var _urlGet = API_URL + '/api/knowledgebase/' + $('#txt_NameKnowledge').val();
            callAjax(_urlGet, "GET").done(function (_resp) {
                if (_resp) {
                    //Create knowledge base in QaNMaker page
                    var _nKnowledge = $('#txt_NameKnowledge').val();
                    createKnowLedgeBase.sendDataToQanMaker(_resp, _nKnowledge);
                }
            }).fail(function (jqXHR, txtStatus, error) {
                //return: server error internal
                console.log('Server error internal: ', jqXHR.statusText);
            });
        });
    },

    sendDataToQanMaker: function (_data, _nKnowledge) {
        var _setting = createKnowLedgeBase.settingCreateKnowledge(_data, _nKnowledge);
        var _URL_QAN = API_QANMAKER + '/create';
        $('#exampleModalCenter').modal({ backdrop: 'static', keyboard: false });
        callAjax(_URL_QAN, "POST", JSON.stringify(_setting), header()).done(function (resp, textStatus, xhr) {
            if (textStatus === 'success' && xhr.status === 202) {
                getKnowledgeBaseByName(_nKnowledge, function (_data) {
                    //update table are column keyId
                    var _urlApi = API_URL + '/api/knowledgebase/updateAreaKeyId';
                    var _dataS = {
                        Area: _nKnowledge,
                        KeyId: _data.id
                    };
                    callAjax(_urlApi, "POST", JSON.stringify(_dataS)).done(function (_response) { });

                    publish(_data.id, function (_resp) {
                        $('#exampleModalCenter').modal('hide');
                        //swal("¡Éxito! El servicio ha sido publicado. ", "You clicked the button!", "success");
                        window.location.reload();
                    });
                });
            }
        }).fail(function (jqXHR, txtStatus, error) {
            //return: server error internal
            console.log('Server error internal: ', jqXHR.statusText);
        });
    },

    validateFilesToSend: function ($fileName) {
        var $filesSend = JSON.parse($('input[name=fileuploader-list-files]').val());
        if ($filesSend.length > 0) {
            for (var index = 0; index < $filesSend.length; index++) {
                var fileName = $filesSend[index].file.replace('0:/', '');
                if (fileName === $fileName) {
                    return true;
                }
            }
        }
    },

    settingCreateKnowledge: function (_data, _nameKnowledge) {
        if (_data) {
            return setting = {
                "name": _nameKnowledge,
                "qnaList": _data
            };
        }
    },
};

//Document ready
$(createKnowLedgeBase.init);