﻿var IdArea = "";
var UserName = "";
var Question = "";
var learningknowledgeBase = {
    //funcion que inicializa el script
    init: function () {

        //Obtiene los parametros desde la URL
        learningknowledgeBase.getParameters();

        $('#btnPublishQuestion').prop("disabled", true);

        //save answer
        $('#btnAddQuestion').on('click', learningknowledgeBase.saveAnswer);

        $('#btnPublishQuestion').on('click', learningknowledgeBase.publishQuestion);
    },

    getParameters: function () {
        //IdArea
        IdArea = learningknowledgeBase.getParametersByURL('IdArea');
        var question = learningknowledgeBase.getParametersByURL('Question');
        Question = learningknowledgeBase.getParametersByURL('Question');
        //UserName = questionAndAnswer.getParameters('UserName');

        if (IdArea !== '' && question !== '') {
            //Obtiene el nombre del area que se va a agregar la respuesta
            var _URL = API_URL + '/api/areas/' + IdArea;
            callAjax(_URL, "GET").done(function (response) {
                if (response) {
                    $('#lbl_nameKnowLedgeBase').text(response.area);
                    $('#Knowledge_KeyID').val(response.keyId);
                }
            }).fail(function (jqXHR, txtStatus, error) {
                //return: server error internal
                console.log('Server error internal: ', jqXHR.statusText);
            });

            $('.bootstrap-tagsinput input').val(question);
            $('.bootstrap-tagsinput input').keypress();
        }
    },

    saveAnswer: function (event) {
        event.preventDefault();
        UserName = learningknowledgeBase.getParametersByURL('UserName');

        //Obtiene la pregunta
        var _questions = $('[data-role=tagsinput]').val();
        //Obtiene la respuesta de la pregunta
        var _answer = $('#txt_answer').val();
        //key id of knowledge base
        var _keyId = $('#Knowledge_KeyID').val();

        if (_answer !== '' && _keyId !== '') {
            //actualiza los datos del base de conocimiento desde el api
            var _dataSetting = learningknowledgeBase.settingSaveAnswer(_answer, _questions);
            var _URL_QAN = API_QANMAKER + '/' + _keyId;
            $('#exampleModalCenter').modal({ backdrop: 'static', keyboard: false });
            callAjax(_URL_QAN, "PATCH", JSON.stringify(_dataSetting), header()).done(function (resp, textStatus, xhr) {
                if (textStatus === 'success' && xhr.status === 202) {
                    //Get URL API
                    var _URL = API_URL + '/api/knowledgebase/saveAnswer';
                    var _data = learningknowledgeBase.creaetObj(_questions, _answer);

                    callAjax(_URL, "POST", JSON.stringify(_data)).done(function (resp, textStatus, xhr) {
                        if (xhr.status === 200) {
                            $('#exampleModalCenter').modal('hide');
                            $('#btnAddQuestion').prop("disabled", true);
                            $('#btnPublishQuestion').prop("disabled", false);
                            //Publica la base de conocimiento con los nuevos cambios
                            //console.log('Publishing knowledge base("' + _keyId + '")...');

                            //publish(_keyId, function (_resp) {
                            //    console.log('Successfully! published knowledge base...');
                            //    $('#exampleModalCenter').modal('hide');
                            //    //swal("¡Éxito! El servicio ha sido publicado. ", "You clicked the button!", "success");

                            //    //send email the notification to user
                            //    var _url = API_URL + '/api/contact/sendMessageToUser/' + UserName;
                            //    callAjax(_url, "GET").done(function (response) {
                            //        if (response) {
                            //            $('#MainContent_txtContactName').val(response.contactName);
                            //            $('#MainContent_txtContactEmail').val(response.email);

                            //            $('#MainContent_txtQuestion').val(Question);
                            //            $('#MainContent_txtAnswer').val(_answer);

                            //            //Evento para enviar Email
                            //            console.log('Sending message...');
                            //            $('.sendEmail').click();
                            //        }
                            //    }).fail(function (jqXHR, txtStatus, error) {
                            //        //return: server error internal
                            //        console.log('Server error internal - Send message: ', jqXHR.statusText);
                            //    });

                            //});
                        }
                    }).fail(function (jqXHR, textStatus, error) {
                        //is return server error
                        console.log('Server error internal - Error save answer: ', error);
                    });
                } else {
                    swal("No fue posible guadar la respuesta, intente de nuevo!", "You clicked the button!", "error");
                }
            }).fail(function (jqXHR, txtStatus, error) {
                //return: server error internal
                console.log('Server error internal: ', jqXHR.statusText);
            });

        }
    },

    publishQuestion: function (event) {
        event.preventDefault();
        UserName = learningknowledgeBase.getParametersByURL('UserName');

        //key id of knowledge base
        var _keyId = $('#Knowledge_KeyID').val();

        //Obtiene la respuesta de la pregunta
        var _answer = $('#txt_answer').val();

        console.log('Publishing knowledge base("' + _keyId + '")...');

        $('#exampleModalCenter').modal({ backdrop: 'static', keyboard: false });

        publish(_keyId, function (_resp) {
            console.log('Successfully! published knowledge base...');
            $('#exampleModalCenter').modal('hide');
            //swal("¡Éxito! El servicio ha sido publicado. ", "You clicked the button!", "success");

            //send email the notification to user
            var _url = API_URL + '/api/contact/sendMessageToUser/' + UserName;
            callAjax(_url, "GET").done(function (response) {
                if (response) {
                    $('#MainContent_txtContactName').val(response.contactName);
                    $('#MainContent_txtContactEmail').val(response.email);

                    $('#MainContent_txtQuestion').val(Question);
                    $('#MainContent_txtAnswer').val(_answer);

                    //Evento para enviar Email
                    console.log('Sending message...');
                    $('.sendEmail').click();
                    $('#exampleModalCenter').modal('hide');
                    window.location = window.location.origin + "" + window.location.pathname;
                }
            }).fail(function (jqXHR, txtStatus, error) {
                //return: server error internal
                console.log('Server error internal - Send message: ', jqXHR.statusText);
            });
        });
    },

    creaetObj: function (_question, _answer) {
        var _obj = {
            idArea: IdArea,
            question: _question.replace(/,/g, ';'),
            answer: _answer,
            user: UserName
        };

        return _obj;
    },

    settingSaveAnswer: function (_answer, _questions) {
        return setting = {
            "add": {
                "qnaList": [
                    {
                        "id": 0,
                        "answer": _answer,
                        "source": "Editorial",
                        "questions": _questions.split(','),
                        "metadata": []
                    }
                ]
            }
        };
    },

    getParametersByURL: function (parameter) {
        parameter = parameter.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + parameter + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
};

$(learningknowledgeBase.init);

