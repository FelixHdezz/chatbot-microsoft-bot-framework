﻿<%@ Page Title="Home Page" Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html lang="en">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><%: Page.Title %> - Easy bot</title>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/bootstrap-theme.css" rel="stylesheet" />
    <link href="Content/Site.css" rel="stylesheet" />
    <link href="https://cdn.botframework.com/botframework-webchat/latest/botchat.css" rel="stylesheet" />


    <script src="Scripts/modernizr-2.8.3.js"></script>
    <script src="Scripts/jquery-3.3.1.js"></script>
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="Content/chat-messeges/chat-messeges.css" rel="stylesheet" />
    <script src="Scripts/plugin/jquery/setup/config.js"></script>
    <script src="Scripts/jquery-3.3.1.js"></script>
    <%--<script src="https://cdn.botframework.com/botframework-webchat/preview/botchat.js"></script>--%>
    <script src="https://cdn.botframework.com/botframework-webchat/latest/botchat.js"></script>

    <script src="Scripts/chat-messeges/chat-messeges.js"></script>

</head>
<body>
    <form runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--To learn more about bundling scripts in ScriptManager see https://go.microsoft.com/fwlink/?LinkID=301884 --%>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="jquery" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a runat="server" href="~/">easy bot</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="container body-content">
            <div class="container text-center">
                <div class="row">
                    <div class="round hollow text-center">
                        <a href="#" id="addClass" style="bottom: 18pt; display: inline; position: fixed; right: 18pt; top: auto;">
                            <span class="glyphicon glyphicon-comment"></span>Open in chat
                        </a>
                    </div>
                </div>
            </div>

            <div class="popup-box chat-popup" id="qnimate">
                <div class="popup-head">
                    <div class="popup-head-left pull-left">                        
                        <img src="Content/Image/ImageIcon.png" alt="iamgurdeeposahan">
                        <span class="head-title">Arca continental</span> 
                    </div>
                    <div class="popup-head-right pull-right">
                        <button data-widget="remove" id="removeClass" class="chat-header-button pull-right" type="button"><i class="glyphicon glyphicon-off"></i></button>
                    </div>
                </div>
                <div class="popup-messages">
                    <div class="direct-chat-messages">
                        <div class="col-md-12 section-chat-messege-welcome">
                            <br />
                            <div class="col-md-12 chat-messeges-saludos" style="text-align: center;">
                                <span>¡Hola! Bienvenido al Asistente<br />
                                    virtual de Arca continental<br />
                                    ¿En qué te puedo ayudar? :)</span><br />
                            </div>

                            <div class="form-group col-md-12" style="margin-top: 2%;">
                                <label for="inputState">Area:</label>
                                <select id="inputState" class="form-control select-area">
                                    <option value="0" selected>Seleccione...</option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <div class="round hollow text-center">
                                    <a href="#" class="render-chat-messeges" id="addClass">Iniciar
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="chat" id="webchat">
                            <%--<iframe src='https://webchat.botframework.com/embed/AsistenteArcaContal?s=lLvp4wLTRxA.cwA.x3g.qgY95ZsLUfyykiunJdnu53oU9Z6E_KdJFKdVFCJqZmU'  style='min-width: 30px; width: 100%; min-height: 330px; border: none;'></iframe>--%>
                        </div>
                    </div>
                </div>
                <div class="popup-messages-footer">
                    <div class="btn-footer">
                        <button class="bg_none pull-right"><i class="glyphicon glyphicon-thumbs-up"></i></button>
                    </div>
                </div>
            </div>
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:Button runat="server" ID="sendEmail" OnClick="sendEmail_Click" CssClass="sendEmail" Style="display: none" />
                    <label runat="server" id="ShowMessage"></label>
                </ContentTemplate>
            </asp:UpdatePanel>
            <hr />
            <footer>
            </footer>
        </div>
        <input runat="server" type="hidden" id="txtUserName" />
        <input runat="server" type="hidden" id="txtUser" />
        <input runat="server" type="hidden" id="txtEmailUser" />
        <input runat="server" type="hidden" id="txtQuestion" />
        <input runat="server" type="hidden" id="txtUrl" />
        <input runat="server" type="hidden" id="txtIdArea" />

        <input runat="server" type="hidden" id="txtContactName" />
        <input runat="server" type="hidden" id="txtContactEmail" />
    </form>
</body>
</html>
