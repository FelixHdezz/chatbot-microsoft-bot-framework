﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.DirectoryServices;
using System.Collections;
using System.DirectoryServices.ActiveDirectory;

public partial class _Default : Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		//Inicializa la clase active directory
		ActiveDirectory activeDirectory = new ActiveDirectory();

		//Al iniciar busca el dominio de la red que esta conectado

		var _userName = activeDirectory.getUserName() != "" ? activeDirectory.getUserName() : "Usuario";

		var dataUser = activeDirectory.getUserTumnail(_userName);

		var _Name = dataUser.Name != null && dataUser.Name != "" ? dataUser.Name : "Usuario"; 

		//Agregar los datos del usuario al header de la pagina
		System.Text.StringBuilder sb = new System.Text.StringBuilder();

		sb.Append(@"<script language='javascript'>");
		//Display name domain conect current
		sb.Append(@"var Name= '" + _Name + "';");
		sb.Append(@"var UserName= '" + _userName.Split('\\').Last() + "';");
		sb.Append(@"var UserEmail= '" + dataUser.Email + "';");
		sb.Append(@"</script>");

		//load to script manager
		ClientScript.RegisterStartupScript(this.GetType(), "JSScript", sb.ToString());
	}

	protected void sendEmail_Click(object sender, EventArgs e)
	{
		ShowMessage.InnerText = "Enviando email...";
		//create model
		MensajeModel _mensajeModel = new MensajeModel();
		_mensajeModel.Name = txtUserName.Value;
		_mensajeModel.UserName = txtUser.Value;
		_mensajeModel.Email = txtEmailUser.Value;
		_mensajeModel.Question = txtQuestion.Value;
		_mensajeModel.IdArea = Convert.ToInt32(txtIdArea.Value);
		_mensajeModel.Url = txtUrl.Value;

		_mensajeModel.ContactName = txtContactName.Value;
		_mensajeModel.ContactEmail = txtContactEmail.Value;

		SendEmail _sendEmail = new SendEmail();

		var result = _sendEmail.SendEmailToQuestion(_mensajeModel);

		ShowMessage.InnerText = result;
	}
}