﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class KnowledgeBase_LearningKnowledgeBase : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{

	}

	protected void sendEmail_Click(object sender, EventArgs e)
	{
		ShowMessage.InnerText = "Enviando correo";
		MensajeModel mensajeModel = new MensajeModel();
		try
		{			
			mensajeModel.Question = txtQuestion.Value;
			mensajeModel.Answer = txtAnswer.Value;

			mensajeModel.ContactName = txtContactName.Value;
			mensajeModel.ContactEmail = txtContactEmail.Value;

			SendEmail _sendEmail = new SendEmail();

			var result = _sendEmail.SendEmailToUser(mensajeModel);
		}
		catch (Exception ex) {
			ShowMessage.InnerText = ex.Message.ToString();
		}
		ShowMessage.InnerText = "Mensaje enviado con exito!";
	}
}