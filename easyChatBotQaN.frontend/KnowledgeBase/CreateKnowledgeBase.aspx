﻿<%@ Page Title="Home Page" MasterPageFile="~/KnowledgeBase/Site.master" AutoEventWireup="true" CodeFile="CreateKnowledgeBase.aspx.cs" Inherits="KnowledgeBase_CreateKnowledgeBase" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="../Content/jquery-fileuploader/jquery-fileuploader.css" rel="stylesheet" />
    <script src="../Scripts/plugin/jquery.fileuploader/jquery.fileuploader.js"></script>
    <script src="../Scripts/knowledge/create-knowledgeBase.js"></script>

    <style>
        .modal-header {
            border-bottom: none !important;
        }

        .modal-footer {
            border-top: none !important;
        }

        @media (min-width: 768px) {
            .modal-dialog {
                margin: 180px auto !important;
            }
        }
    </style>
    <div class="card fat">
        <div class="card-body">
            <h2 class="card-title" id="tituloForm">Crear base de conocimiento</h2>
            <br />
            <hr />
            <form id="uploadForm" enctype="multipart/form-data">
                <div class="row">
                    <div class="row">
                        <p style="font-weight: 600;">Area responsable</p>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="f_nombre" style="font-weight: 600;">Nombre responsable</label>
                                <input type="text" class="form-control" id="txt_nameResponsable" tabindex="1" name="NameResponsable" required maxlength="50" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="f_nombre" style="font-weight: 600;">Apellidos</label>
                                <input type="text" class="form-control" id="txt_lastName" tabindex="1" name="Apellidos" required maxlength="120" />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="f_nombre" style="font-weight: 600;">Telefono</label>
                                <input type="text" class="form-control" id="txt_phone" tabindex="1" name="Telefono" required maxlength="10" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="f_nombre" style="font-weight: 600;">Email</label>
                                <input type="text" class="form-control" id="txt_email" tabindex="1" name="Email" required maxlength="120" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <br />
                        <p style="font-weight: 600;">Base conocimiento</p>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="f_nombre" style="font-weight: 600;">Nombre</label>
                                <input type="text" class="form-control" id="txt_NameKnowledge" tabindex="1" name="question" required maxlength="600" />
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group" id="uploadFiles">
                                <input type="file" name="files">
                                <div class="form-group text-right btnSendFiles">
                                    <button type="submit" id="btnUploadSubmit" class="btn btn-success">Agregar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                </div>
                <div class="modal-body">
                    <div class="image-loading" style="margin:auto; text-align:center;">
                        <img src="../Content/Image/big-loading-gif.gif" width="70px" />
                    </div>  
                    <div class="message-loading" style="text-align:center; padding-top:7%; font-size:18px;">
                        <p>Configurando servicio, esto tomara unos minutos ... </p>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
