﻿<%@ Page Title="Home Page" MasterPageFile="~/KnowledgeBase/Site.master" AutoEventWireup="true" CodeFile="LearningKnowledge.aspx.cs" Inherits="KnowledgeBases_LearningKnowledge" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
    <script src="../Scripts/knowledge/learningKnowledgeBase.js"></script>
    <style>
        .bootstrap-tagsinput {
            height: 47px;
            width: 100% !important;
            padding: 11px 6px !important;
        }

        .label-info {
            background-color: none !important;
        }

        .bootstrap-tagsinput .tag {
            color: black !important;
        }

        .label {
            font-size: 100% !important;
            font-weight: normal !important;
        }

        .modal-header {
            border-bottom: none !important;
        }

        .modal-footer {
            border-top: none !important;
        }

        @media (min-width: 768px) {
            .modal-dialog {
                margin: 180px auto !important;
            }
        }
    </style>

    <div class="card fat">
        <div class="card-body">
            <h2 class="card-title" id="tituloForm">Agregar Respuesta</h2>
            <br />
            <hr />
            <form runat="server" id="albumForm" novalidate>
                <asp:ScriptManager runat="server">
                    <Scripts>
                        <%--To learn more about bundling scripts in ScriptManager see https://go.microsoft.com/fwlink/?LinkID=301884 --%>
                        <%--Framework Scripts--%>
                        <asp:ScriptReference Name="MsAjaxBundle" />
                        <asp:ScriptReference Name="jquery" />
                        <asp:ScriptReference Name="bootstrap" />
                        <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                        <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                        <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                        <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                        <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                        <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                        <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                        <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                        <asp:ScriptReference Name="WebFormsBundle" />
                        <%--Site Scripts--%>
                    </Scripts>
                </asp:ScriptManager>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            Base de conocimiento : 
                            <label for="lbl_nameKnowLedgeBase" id="lbl_nameKnowLedgeBase" style="font-weight: 600;">Default</label>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <input type="hidden" data-custom-type="id" id="f_id" name="id" value="0" />
                        <div class="form-group">
                            <label for="f_nombre" style="font-weight: 600;">Pregunta</label>

                            <input type="text" value="" data-role="tagsinput" style="display: none;" />

                            <%--<input type="text" class="form-control" id="txt_question" tabindex="1" name="question" required maxlength="600" />--%>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="f_tipoFuenteId" style="font-weight: 600;">Respuesta</label>
                            <input type="text" class="form-control" id="txt_answer" tabindex="1" name="answer" required maxlength="600" />
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-right">
                        <div class="form-group no-margin">
                            <button type="submit" id="btnAddQuestion" class="btn btn-success">Agregar</button>
                            <button type="submit" id="btnPublishQuestion" class="btn btn-primary">Publicar</button>
                        </div>
                    </div>
                </div>
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        
                    </ContentTemplate>
                </asp:UpdatePanel>
            </form>
            <input type="text" value="" id="Knowledge_KeyID" style="display: none;" />

            <input runat="server" type="hidden" id="txtQuestion" />
            <input runat="server" type="hidden" id="txtAnswer" />

            <input runat="server" type="hidden" id="txtContactName" />
            <input runat="server" type="hidden" id="txtContactEmail" />
        </div>
    </div>

    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                </div>
                <div class="modal-body">
                    <div class="image-loading" style="margin: auto; text-align: center;">
                        <img src="../Content/Image/big-loading-gif.gif" width="70px" />
                    </div>
                    <div class="message-loading" style="text-align: center; padding-top: 7%; font-size: 18px;">
                        <p>Configurando servicio, esto tomara unos minutos ... </p>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

</asp:Content>
