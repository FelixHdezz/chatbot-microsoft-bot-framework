﻿<%@ Page Title="Home Page" MasterPageFile="~/KnowledgeBase/Site.master" AutoEventWireup="true" CodeFile="AddKnowledge.aspx.cs" Inherits="KnowledgeBases_AddKnowledge" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="card fat">
        <div class="card-body">
            <h2 class="card-title" id="tituloForm">Agregar base de conocimento</h2>
            <br />
            <hr />
            <form id="albumForm" novalidate>
                <div class="row">
                    <div class="col-sm-12">
                        <input type="hidden" data-custom-type="id" id="f_id" name="id" value="0" />
                        <div class="form-group">
                            <label for="f_nombre" style="font-weight: 600;">Pregunta</label>
                            <input type="text" class="form-control" id="txt_question" tabindex="1" name="question" required maxlength="600" />
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="f_tipoFuenteId" style="font-weight: 600;">Respuesta</label>
                            <input type="text" class="form-control" id="txt_answer" tabindex="1" name="answer" required maxlength="600" />
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-right">
                        <div class="form-group no-margin">
                            <button type="submit" id="albumSubmitBtn" class="btn btn-success">Agregar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</asp:Content>